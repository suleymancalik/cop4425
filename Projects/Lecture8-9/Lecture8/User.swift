//
//  User.swift
//  Lecture8
//
//  Created by Suleyman Calik on 3.11.2014.
//  Copyright (c) 2014 BAU. All rights reserved.
//

import Foundation
import CoreData

class User: NSManagedObject {

    @NSManaged var username: String
    @NSManaged var password: String
    @NSManaged var userId: NSNumber

}
