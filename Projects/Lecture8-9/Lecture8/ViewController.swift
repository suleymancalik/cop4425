//
//  ViewController.swift
//  Lecture8
//
//  Created by Suleyman Calik on 3.11.2014.
//  Copyright (c) 2014 BAU. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var txtRegisterUsername: UITextField!
    @IBOutlet weak var txtRegisterPassword: UITextField!
    @IBOutlet weak var txtRegisterPasswordAgain: UITextField!
    
    @IBOutlet weak var txtLoginUsername: UITextField!
    @IBOutlet weak var txtLoginPassword: UITextField!
    
    var context:NSManagedObjectContext!
    var userEntity:NSEntityDescription!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtRegisterUsername.delegate = self
        txtRegisterPassword.delegate = self
        txtRegisterPasswordAgain.delegate = self
        txtLoginUsername.delegate = self
        txtLoginPassword.delegate = self
        
        
        var appDelegate  = UIApplication.sharedApplication().delegate as AppDelegate
        context = appDelegate.managedObjectContext
        userEntity = NSEntityDescription.entityForName("User",inManagedObjectContext:context)
        
    }
    
    // MARK: - Textfield Methods
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        
        if textField == txtLoginUsername || textField == txtLoginPassword {
            
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.frame.origin.y = -230
            })
        }
    }

    
    //MARK: - Action Methods
    
    func closeKeyboard() {
        txtLoginPassword.resignFirstResponder()
        txtLoginUsername.resignFirstResponder()
        txtRegisterUsername.resignFirstResponder()
        txtRegisterPassword.resignFirstResponder()
        txtRegisterPasswordAgain.resignFirstResponder()
        
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.view.frame.origin.y = 0
        })

    }
    
    @IBAction func backgroundTapped(sender: AnyObject) {
        closeKeyboard()
    }
    
    @IBAction func actionRegister(sender: AnyObject) {
        
        if !txtRegisterUsername.text.isEmpty {
            if !txtRegisterPassword.text.isEmpty {
                if !txtRegisterPasswordAgain.text.isEmpty {
                    
                    if txtRegisterPassword.text == txtRegisterPasswordAgain.text {

                        var newUser = User(entity:userEntity, insertIntoManagedObjectContext:context)
                        newUser.username = txtRegisterUsername.text
                        newUser.password = txtRegisterPassword.text
                        var isRegistered = context.save(nil)
                        if isRegistered {
                            println("User registered")
                        }
                        else {
                            println("User NOT registered")
                        }
                        
                    } else { println("Passwords are not matching!") }
                } else { println("Please enter your password again!") }
            } else { println("Please enter your password!") }
        } else { println("Please enter your username!") }
        
        
    }
    
    
    @IBAction func actionLogin(sender: AnyObject) {
        
        if !txtLoginUsername.text.isEmpty && !txtLoginPassword.text.isEmpty {
            println("Login User")
            
            var request = NSFetchRequest(entityName: "User")
            request.returnsObjectsAsFaults = false
            request.predicate = NSPredicate(format:"(username == %@) AND (password == %@)"
                ,txtLoginUsername.text , txtLoginPassword.text)
            var users = context.executeFetchRequest(request, error: nil)
            
            if users?.count > 0 {
                println("LOGGED IN SUCCESSFULLY")
                // login success
            }
            else {
                println("WRONG USERNAME OR PASSWORD!!!")
            }
            
        }
        else {
            println("Please fill all textfields!")
        }
        
        
    }
    
    
    

}










