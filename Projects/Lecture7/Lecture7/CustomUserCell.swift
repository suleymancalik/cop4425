//
//  CustomUserCell.swift
//  Lecture7
//
//  Created by Suleyman Calik on 27.10.2014.
//  Copyright (c) 2014 BAU. All rights reserved.
//

import UIKit

class CustomUserCell: UITableViewCell {

    @IBOutlet weak var lblUsername: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
