//
//  ViewController.swift
//  Lecture7
//
//  Created by Suleyman Calik on 27.10.2014.
//  Copyright (c) 2014 BAU. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var selectedRow : Int = -1
    var deselectedRow : Int = -1
    
    var users = [
        "Suleyman Calik" ,
        "Abdulbaki Kocak" ,
        "Al Hareth Altalib" ,
        "Armen Murat Aktar"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.dataSource = self
        tableView.delegate = self
    }
    

    //MARK: - TableView Methods
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        println("Number of cells: \(users.count)")
        return users.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell!
        
        if indexPath.row % 2 == 0 {
            var customCell = tableView.dequeueReusableCellWithIdentifier("CustomUserCell") as CustomUserCell
            customCell.lblUsername.text = users[indexPath.row]
            cell = customCell
        }
        else {
            cell = tableView.dequeueReusableCellWithIdentifier("UserCell") as UITableViewCell
            cell.textLabel.text = users[indexPath.row]
        }
        
        if selectedRow == indexPath.row {
            cell.backgroundColor = UIColor.redColor()
        }
        else if deselectedRow == indexPath.row {
            cell.backgroundColor = UIColor.yellowColor()
        }
        else {
            cell.backgroundColor = UIColor.greenColor()
        }
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        var user = users[indexPath.row]
        var alertController = UIAlertController(title: "Selected User", message: user, preferredStyle: UIAlertControllerStyle.Alert)
        
        var alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default) { (action) -> Void in
            print("User Pressed OK")
        }
        alertController.addAction(alertAction)
        
//        self.presentViewController(alertController, animated:true, completion:nil)
        
        selectedRow = indexPath.row
//        tableView.reloadData()
    }

    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        deselectedRow = indexPath.row
        tableView.reloadData()
    }
}









