// Playground - noun: a place where people can play

import UIKit

enum CompassPoint {
    case North
    case South
    case East
    case West
}

var direction = CompassPoint.North

switch direction {
case CompassPoint.North:
    println("This is North!")
default:
    println("This is another direction")
}


enum Barcode {
    case UPCA(Int,Int)
    case QRCode(String)
}

var qrCode = Barcode.QRCode("bla bla")
var upcaCode = Barcode.UPCA(123, 456)


switch qrCode {
case .UPCA(let productNo, let companyNo):
    println("qwertyuio")
case .QRCode(let content):
    println(content)
}



// EXTENSIONS

extension Int {
    func printDescription() {
        println("I am \(self)")
    }
}

10.printDescription()











