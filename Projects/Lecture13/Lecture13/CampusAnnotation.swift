//
//  CampusAnnotation.swift
//  Lecture13
//
//  Created by Suleyman Calik on 08/12/14.
//  Copyright (c) 2014 BAU. All rights reserved.
//

import UIKit
import MapKit

class CampusAnnotation: NSObject , MKAnnotation {
   
    var coordinate:CLLocationCoordinate2D
    var title:String!
    var subtitle:String!
    
    init(coordinate:CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}




