//
//  ViewController.swift
//  Lecture13
//
//  Created by Suleyman Calik on 08/12/14.
//  Copyright (c) 2014 BAU. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController , CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapview: MKMapView!
    var locationManager = CLLocationManager()
    
//    CLLocationCoordinate2D(latitude:41.041994, longitude: 29.009049)
//    CLLocationCoordinate2D(latitude: 41.0243174, longitude: 28.9762317)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        mapview.delegate = self
        
        showCampuses()
    }
    
    
    func showCampuses() {
   
        var besiktasCoord = CLLocationCoordinate2D(latitude:41.041994, longitude: 29.009049)
        var besiktasCampus = CampusAnnotation(coordinate: besiktasCoord)
        besiktasCampus.title = "Bahcesehir Uni"
        besiktasCampus.subtitle = "Besiktas Campus"
        mapview.addAnnotation(besiktasCampus)
        
        
        var galataCoord = CLLocationCoordinate2D(latitude: 41.0243174, longitude: 28.9762317)
        var galataCampus = CampusAnnotation(coordinate: galataCoord)
        galataCampus.title = "Bahcesehir Galata Kampus"
        mapview.addAnnotation(galataCampus)
    }
    
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        
        if annotation is CampusAnnotation {
            let identifier = "IDForAnnotation"
            var annView  = mapview.dequeueReusableAnnotationViewWithIdentifier(identifier)
            if annView == nil {
                annView = MKAnnotationView(annotation:annotation, reuseIdentifier: identifier)
                
                annView.rightCalloutAccessoryView = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as UIView
            }
            annView.canShowCallout = true
            annView.image = UIImage(named: "bu")
            
            return annView
        }
        else {
            return nil
        }

    }
    
    
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) {
        
        println(view.annotation.title!)
    }

    
    // MARK: - Mapview Methods
    
    func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!) {
        
        if let location = userLocation.location {
         
            var coordinate = location.coordinate
            
            
            var region = MKCoordinateRegionMakeWithDistance(coordinate, 5000, 5000)
            mapview.setRegion(region, animated: true)
            
            
            println("User Location: \(coordinate.latitude) \(coordinate.longitude)")
        }
    }
    
    
    // MARK: - LocationManager Methods
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
        case .NotDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .Denied , .Restricted:
            println("We can't fetch location!")
        case .Authorized , .AuthorizedWhenInUse:
            mapview.showsUserLocation = true
            
        }
        
    }

}



