// Playground - noun: a place where people can play

import UIKit

struct Triangle {
    static var numberOfSides = 3
    var sideLength:Double = 0.0
    var perimeter : Double {
        get {
            return sideLength * 3.0
        }
        set {
            sideLength = newValue / 3.0
        }
    }
}

// usage of triangle

Triangle.numberOfSides

var myTriangle = Triangle(sideLength:10)
myTriangle.perimeter
myTriangle.sideLength


myTriangle.perimeter = 60

myTriangle.sideLength
myTriangle.perimeter




// AUDIO CHANNEL

struct AudioChannel {
    static let maxLevel = 10
    
    var currentLevel:Int = 0 {
        didSet {
            if currentLevel > AudioChannel.maxLevel {
                currentLevel = AudioChannel.maxLevel
            }
            else if currentLevel < 0 {
                currentLevel = 0
            }
        }
    }
}


var leftChannel = AudioChannel()

leftChannel.currentLevel
leftChannel.currentLevel = 5
leftChannel.currentLevel


leftChannel.currentLevel = 123456
leftChannel.currentLevel

leftChannel.currentLevel = -123
leftChannel.currentLevel


// SUBSCRIPTS


struct TimesTable {
    let multiplier:Int
    
    subscript(value:Int) -> Int {
        
        return multiplier * value
    }
}

var threeTimesTable = TimesTable(multiplier:3)
threeTimesTable[12345]
//threeTimesTable[9]





















