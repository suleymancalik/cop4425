//
//  ViewController.swift
//  Lecture12
//
//  Created by Suleyman Calik on 1.12.2014.
//  Copyright (c) 2014 BAU. All rights reserved.
//

import UIKit
import MapKit


class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    var locationManager = CLLocationManager()    
    var mapView = MKMapView()
    var segmentControl = UISegmentedControl(items: ["Standard" , "Satellite" , "Hybrit"])
    var slider = UISlider()
    var distance:Double = 5_000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showMapView()
        runLocationProvider()
    }
    
    func runLocationProvider() {
        
        locationManager.delegate = self        
    }
    
    
    func showMapView() {
        var margin:CGFloat = 20.0
        var width = self.view.frame.width - (2 * margin)
        mapView.frame = CGRect(x:margin, y:margin, width:width, height:width)
        mapView.delegate = self
        mapView.zoomEnabled = false
        self.view.addSubview(mapView)
        
        
        var x:CGFloat = (self.view.frame.width - segmentControl.frame.width) / 2.0
        segmentControl.frame.origin = CGPoint(x: x, y: margin + mapView.frame.origin.y + mapView.frame.size.height)
        
        segmentControl.addTarget(self, action: "segmentValueChanged", forControlEvents: UIControlEvents.ValueChanged)
        segmentControl.selectedSegmentIndex = 0
        self.view.addSubview(segmentControl)
        
        
        slider.frame = CGRect(x: margin, y: segmentControl.frame.origin.y + segmentControl.frame.height + margin, width: mapView.frame.width, height: 30)
        slider.addTarget(self, action: "sliderValueChanged", forControlEvents: UIControlEvents.ValueChanged)
        
        slider.minimumValue = 500
        slider.maximumValue = 50_000
        
        slider.value = Float(distance)
        slider.enabled = false
        
        self.view.addSubview(slider)
    }
    
    
    func sliderValueChanged() {
        distance = Double(slider.value)
        changeMapZoom(false)
    }
    
    
    func segmentValueChanged() {

        switch segmentControl.selectedSegmentIndex {
        case 0:
            mapView.mapType = MKMapType.Standard
        case 1:
            mapView.mapType = MKMapType.Satellite
        case 2:
            mapView.mapType = MKMapType.Hybrid
        default:
            break
        }
    }
    

    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
        case .NotDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .Restricted, .Denied:
            println("Location Manager NOT ENABLED!")
        case .Authorized, .AuthorizedWhenInUse:
            mapView.showsUserLocation = true
        }
    }

    
    
    func mapView(mapView: MKMapView!, didUpdateUserLocation userLocation: MKUserLocation!) {
        
        if let location:CLLocation = userLocation.location {
            
            slider.enabled = true
            changeMapZoom(true)
        }
    }
    
    
    func changeMapZoom(animated:Bool) {
        var region = MKCoordinateRegionMakeWithDistance(mapView.userLocation.location.coordinate, distance, distance)
        mapView.setRegion(region, animated:animated)
    }

}










