//
//  ViewController.swift
//  Lecture4
//
//  Created by Suleyman Calik on 13/10/14.
//  Copyright (c) 2014 Bahcesehir University. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var txtUsername: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func actionLogin(sender: AnyObject) {
        
        if !txtUsername.text.isEmpty {
            
            println("Hosgeldiniz: \(txtUsername.text)")
        }
        
    }
    
    
    


}






