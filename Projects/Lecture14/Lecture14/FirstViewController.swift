//
//  FirstViewController.swift
//  Lecture14
//
//  Created by Suleyman Calik on 15/12/14.
//  Copyright (c) 2014 BAU. All rights reserved.
//

import UIKit


var users = Array<AnyObject>()


class FirstViewController: UIViewController {

    @IBOutlet weak var txtUserId: UITextField!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    
    @IBAction func getUserFromFacebook(sender: UIButton) {
        
        if !txtUserId.text.isEmpty {
            
            // Creating url String
            var urlString = "http://graph.facebook.com/\(txtUserId.text)/"
            
            // Creating and cheking NSURL object
            if let url = NSURL(string: urlString) {
                
                // Creating Requet Object for URL
                var request = NSURLRequest(URL: url)
                
                // Running request in background
                NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue(), completionHandler: { (response:NSURLResponse!, data:NSData!, error:NSError!) -> Void in
                    
                    // Checking if has error or not
                    if error == nil {
                        
                        var err:NSError?
                        var jsonObject:Dictionary<String,String>! = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: &err) as? Dictionary<String,String>
                        println(jsonObject)
                        
                        if jsonObject != nil {
                            self.lblName.text = jsonObject["name"]
                            self.lblGender.text = jsonObject["gender"]
                            
                            users.append(jsonObject)
                        }
                        else {
                            self.lblName.text = "NO USER"
                            self.lblGender.text = ""
                        }
                        
                    }
                    else {
                        println("Can not fetch user: \(error!)")
                    }
                })
                
            }
            
        }
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

