//
//  SecondViewController.swift
//  Lecture14
//
//  Created by Suleyman Calik on 15/12/14.
//  Copyright (c) 2014 BAU. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITableViewDataSource {

    
    @IBOutlet weak var tblUsers: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblUsers.dataSource = self
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        tblUsers.reloadData()
    }
    


    // MARK: - Table Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCellWithIdentifier("UserCell") as UITableViewCell
        
        var userDict:Dictionary<String,String> = users[indexPath.row] as Dictionary<String,String>
        
        cell.textLabel?.text = userDict["name"]
        
        return cell
    }

}

